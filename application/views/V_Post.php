<div class="row">
    <div class="col-md-12">
            <?php if ($userid) { ?>
            	<form method="post" action="<?php echo base_url('C_Lumut/CreatePost') ?>">
            		<input type="text" name="title" class="form-control" placeholder="Title"><br>
            		<textarea name="content" class="form-control" placeholder="Content"></textarea>
            		<button type="submit" class="btn">Insert</button>
            	</form>
            <?php } ?>
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        Title
                    </th>
                    <th>
                        Content
                    </th>
                    <th>
                        Date
                    </th>
                    <th>
                        Username
                    </th>

                    <th>
                        action
                    </th>
                </tr>
            </thead>
            <tbody>
            	<?php $no=1; foreach ($post as $line) { ?>
                <tr>
                	<td><?php echo $no++; ?></td>
                	<td><?php echo $line['title']; ?></td>
                	<td><?php echo $line['content']; ?></td>
                	<td><?php echo $line['date']; ?></td>
                	<td><?php echo $line['username']; ?></td>
                	<td>
                		<a href="<?php echo base_url('C_Lumut/Delete/'.$line['idpost']) ?>">delete</a>
                	</td>
                </tr>
            	<?php } ?>
            </tbody>
        </table>
    </div>
</div>