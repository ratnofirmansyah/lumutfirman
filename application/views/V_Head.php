<!DOCTYPE html>
<html>
<head>
	<title>Firman Application</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
</head>
<body>
	 <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Firman Apps</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo base_url() ?>">Home</a></li>
            <li><a href="<?php echo base_url('C_Lumut/Post') ?>">Post</a></li>
            <li><a href="#contact">AKun</a></li>
            <?php if ($userid) { ?>
            	<li><a href="<?php echo base_url('C_Lumut/Logout') ?>">Logout (<?php echo $userid; ?>)</a></li>
            <?php }else{ ?>
            	<li><a href="<?php echo base_url('C_Lumut/Login') ?>">Login</a></li>
            <?php } ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div style="padding-top: 100px" class="container">