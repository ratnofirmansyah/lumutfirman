<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Lumut extends CI_Controller {
	function __construct() {
	    parent::__construct();

	    $this->load->helper('url');
	    $this->load->library('session');
	    $this->load->model('M_lumut');
	}

	public function index()
	{
		$dt['userid'] = $this->session->username;
		$this->load->view('V_Head', $dt);
		$this->load->view('V_Home');
		$this->load->view('V_Footer');
	}

	public function Login()
	{
		$dt['userid'] = $this->session->username;
		$this->load->view('V_Head', $dt);
		$this->load->view('V_Login');
		$this->load->view('V_Footer');
	}

	public function DoLogin()
	{
		$u = $this->input->post('username');
		$p = $this->input->post('password');

		$check = $this->M_lumut->Login($u,$p);
		if ($check > 0) {
			$sessiondata = array('username' => $u, );
			$this->session->set_userdata($sessiondata);
			redirect('');
		}else{
			redirect('C_Lumut/Login');
		}

	}

	public function Post()
	{
		$a['userid'] = $this->session->username;
		$a['post'] = $this->M_lumut->readPost();
		$this->load->view('V_Head', $a);
		$this->load->view('V_Post', $a);
		$this->load->view('V_Footer');
	}

	public function CreatePost()
	{
		$dt = array(
			'title' => $this->input->post('title'),
			'content' => $this->input->post('content'),
			'date' => date('Y-m-d H:i:s'),
			'username' => $this->session->username,
		);
		// print_r($dt);
		$this->M_lumut->insertPost($dt);
		// exit();
		redirect('C_Lumut/Post');
	}

	public function Logout()
	{
		$this->session->unset_userdata('username');
		redirect('');
	}

	public function Delete($id)
	{
		$this->M_lumut->deletePost($id);
		redirect('C_Lumut/Post');
	}

}
